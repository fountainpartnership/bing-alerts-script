# Bing Alerts Script

## Slack Script, 2-Step Setup:
### 1. Paste Your Spreadsheet URL, Slack Channel Name, and Slack Webhook URL into the Spreadsheet URL Section of the Script File:
![Script_Copy_and_Paste](https://docs.google.com/drawings/d/e/2PACX-1vRI0LFm2RoUVDNt9osrO5T0YpGyZkPK570PyYe53ODnkSLxv8J6L824E84uTlXkCEXcfQAeiWmz8w0r/pub?w=800&h=510)
### 2.  Schedule the Script to Run on You Bing Ads Script Account:
![Bing_Scheduling_Action](https://docs.google.com/a/fountainpartnership.co.uk/drawings/d/spDxTre0y-iMXBc3e2ipDLw/image?w=620&h=510&rev=15&ac=1&parent=1ycW2NnsTUwgIZllNxjE8A--WL0T7ydA-zv0AD6OMVCA)
