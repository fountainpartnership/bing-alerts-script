function main() {
    
    let spreadsheetUrl = 'SETTINGS_SHEET_URL_GOES_HERE';
    let slack_channel_name = 'SLACK_CHANNEL_NAME_GOES_HERE';
    let slack_webhook_url = 'WEBHOOK_ADDRESS_GOES_HERE';
    let reportingConfig = parseCsv(spreadsheetUrl, true);
    try 
    {
        // if the script is located within an agency shell, this will not throw an error.
        var accounts = AccountsApp.accounts().get();
        
        var reportObject = new Report(reportingConfig, true);
    } catch(e) 
    {
        var reportObject = new Report(reportingConfig, false);
    }
    if (!reportObject.isAgencyShellReport) 
    {
        var report = reportObject.reportValues;
        var accountName = report.accountName ? report.accountName : false;
        var rows = report.reportRows;
        var msg = accountName ? `*${accountName} Bing Ads Report:*\n\n${rows.join("\n")}` : "No alerts object has been created";
        sendSlackMessage(msg, slack_channel_name, slack_webhook_url);
    }
    else
    {
        var reportObject = reportObject.reportValues;
        var report = ["*Agency Shell Alerts:*"];
        for (var i = 0 ; i < reportObject.length; i++) 
        {
            var accountReport = reportObject[i];
            var accountName = report.accountName ? report.accountName : false;
            if (accountName) 
            {
                var reportRows = accountReport.reportRows;
                reportRows.unshift(`*_${accountName}:_*`);
                reportRows = reportRows.join("\n");
                report.push(reportRows); 
            }
        }
        sendSlackMessage(accountName ? report.join("\n") : "No alerts object has been created", slack_channel_name, slack_webhook_url);
        
    }
}

function Report(config, agencyShell) 
{
    // if the script is running within the Agency shell, then the reporting object
    // stores an array of the reports as it iterates through accounts.
    if (agencyShell) 
    {
        this.isAgencyShellReport = true;
        this.listOfReports = [];
        for (accountName in config) 
        {
            this.listOfReports.push(checkAccountAgainstMetrics(accountName, config));
        }
        this.reportValues = this.listOfReports;

        delete this.listOfReports; 
    }
    else 
    {
        this.reportValues = checkAccountAgainstMetrics('Current Account', config);
    }
    this.build = function () 
    {
        if (this.isAgencyShellReport) 
        {
            retVal = 
            {
                isAgencyShellReport: true,
                reportValues: this.report
            } 
        }
        else 
        {
            retVal = 
            {
                reportValues: this.report
            }
        }
        return retVal;
    }

    function checkAccountAgainstMetrics(accountName, config) 
    {
        let retVal = {reportRows: []};
        if (accountName in config) 
        {
            let account = config[accountName];
            for (metric in account)
            {
                let settings = account[metric];
                switch (settings.entityType) 
                {
                    case 'Campaign' :
                        var entities = BingAdsApp.campaigns().withCondition("DeliveryStatus = ELIGIBLE").forDateRange(settings.dateRange).get();
                        break;
                    case 'Ad Group' :
                        var entities = BingAdsApp.adGroups().withCondition("CampaignStatus = ENABLED").forDateRange(settings.dateRange).get();
                        break;
                    case 'Keyword':
                        var entities = BingAdsApp.keywords().withCondition("CampaignStatus != ENABLED").forDateRange(settings.dateRange).get();
                        break;
                    default :
                        try
                        {
                            var entities = AccountsApp.accounts().withCondition(`Name = ${accountName}`).forDateRange(settings.dateRange).get();
                        } catch (e) 
                        {
                            throw 'The script is running within a Bing account and under an agency shell therefore certain functions cannot be accessed.'
                        }
                        
                }
                while (entities.hasNext()) 
                {
                    let entity = entities.next();
                    if (settings.entityType !== "Keyword")
                    {
                        var entityName = entity.getName();
                    } else 
                    {
                        var entityName = entity.getText();
                    }
                    
                    let entityType = settings.entityType;
                    let stats = entity.getStats();

                    switch (metric)
                    {
                        case 'Average Cpc' : 
                            var stat = stats.getAverageCpc();
                            break;
                        case 'Average Cpm' :
                            var stat = stats.getAverageCpm();  
                            break;
                        case 'Average Position' :
                            var stat = stats.getAveragePosition();
                            break;
                        case 'Click Conversion Rate' :
                            var stat = stats.getClickConversionRate();
                            break;
                        case 'Clicks' :
                            var stat = stats.getClickConversionRate();
                            break;
                        case 'Converted Clicks' :
                            var stat = stats.getConvertedClicks();
                            break;
                        case 'Cost' : 
                            var stat = stats.getCost();
                            break;
                        case 'Ctr' :
                            var stat = stats.getCtr();
                            break;
                        case 'Impressions' :
                            var stat = stats.getImpressions();
                            break;
                        default :
                            throw `the stat chosen for one of your rows is invalid. The metric '${metric}' cannot be queried in Bing Ads Script`;
                    }

                    var evaluationStatement = 
                    [
                        'if (',
                        'stat ',
                        settings.operator,
                        settings.value,
                        ') ',
                        'retVal.reportRows.push(',
                        '"_The metric \''+metric+'\' has reached a value of '+stat+', outside of your accepted range for the '+entityType+' '+entityName+'_"',
                        ');'
                    ];
                    retVal.accountName = BingAdsApp.currentAccount().getName();
                    eval(evaluationStatement.join(""));
                } 
            }
        }
        return retVal;
    }
}

function parseCsv(url, row1HasHeaders) 
{
    let retVal = {};
    let fetch = UrlFetchApp.fetch(url);
    
    if (fetch.getResponseCode() == 200) 
    {
        var csvContent = fetch.getContentText().split("\n");
    }
    var rows = new csviterator(csvContent);
    if (row1HasHeaders) 
    {
        // skip the first row
        let headers = rows.next();
    }

    while (rows.hasNext()) 
    {
        let row = rows.next();
        let [accountName, entityType, metric, dateRange, operator, value] = row;

        if (accountName == "") accountName = 'Current Account';
        
        if (row.indexOf("") < 1) 
        {
            if (!(accountName in retVal))
            {
                retVal[accountName] = {};
            }
            retVal[accountName][metric] =
            {
                entityType:entityType,
                dateRange:dateRange,
                operator:operator,
                value:value
            }
        }
    }

    if (Object.keys(retVal).length > 0) 
    {
        return retVal;
    } else 
    {
        throw `The correct settings could not be be found in your spreadsheet. Please check your url: ${url}`;
    }

    function csviterator(csvContent) 
    {
        let rows = csvContent;
        this.hasNext = function() 
        {
            return rows.length > 0;
        }
        this.next = function() 
        {
            if (rows.length > 0) 
            {
                let retVal = rows[0].split(",");
                rows.splice(0,1);
                return retVal;
            } else 
            {
                throw "The iterator has reached its end.";
            }
        }
    }
}

function sendSlackMessage(text, opt_channel, SLACK_URL) {
  
  let slackMessage = {
    text: text,
    username: 'Bing Ads Script',
    link_names: 1,
    channel: opt_channel || '#general'
  };

  let options = {
    method: 'POST',
    contentType: 'application/json',
    payload: JSON.stringify(slackMessage)
  };
  var errors = '';
  try {
      var errors = UrlFetchApp.fetch(SLACK_URL, options);
      errors.getContentText();
  }
  catch (e) 
  {
      Logger.log(errors);
      throw e;
  }
}